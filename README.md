# Envision Android Assignment

## Introduction

The Envision Library is a feature within the Envision app that users frequently utilize after scanning documents. It provides the convenience of storing scanned documents within the app in an accessible manner. This task involves a simple re-implementation of that feature.

## Design

The design for this feature can be found through this Figma link: [Envision Assignment Design](https://www.figma.com/file/mc8Ahvua2SU1sSpFLdchOp/%F0%9F%94%B5-Assignments?node-id=1%3A1011). Instructions on how to approach the UX for this assignment are provided on the Figma page itself.

## Technical Requirements

1. **OCR Setup**: Use the following POST endpoint for OCR: [Read Document API](https://letsenvision.app/api/test/readDocument).

   Example usage of the API endpoint:
   ```sh
   curl --location --request POST 'https://letsenvision.app/api/test/readDocument' \
   --header 'Cookie: __cfduid=d97604b6c67574ccd048c013ffbee703a1614774197' \
   --form 'photo=@/Users/johndoe/Desktop/Screenshot 2021-02-25 at 23.50.47.png'
   ```

2. **Accessibility**: Ensure that the app is fully accessible using Talkback. If you are not familiar with Android Accessibility, start here: [Android Accessibility Codelab](https://developer.android.com/codelabs/starting-android-accessibility).

3. **Camera Implementation**: Use CameraX for the camera functionality.

4. **Local Storage**: Library files should be saved locally.

5. **Development**: Implement the assignment in Kotlin, and use the latest available libraries, such as Jetpack Compose and dependency injection.

## Bonus Points

1. **Multilingual Support**: Implement support for different languages and the ability to read out documents with multiple languages.

## Submission

Fork the provided repository and submit your assignment. The deadline for the assignment is one week. If you need more time, please inform your contact at Envision.

For any questions regarding the assignment, please contact kk@letsenvision.com.